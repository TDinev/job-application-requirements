from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time


def start():
    driver = webdriver.Firefox()
    driver.get("https://www.glassdoor.com/Job/index.htm")
    #driver.get("https://www.glassdoor.com/Job/biostatistical-intern-jobs-SRCH_KE0,21_IP5.htm")
    scrapGlassdoor(driver, "biostatistician", "us")


def scrapGlassdoor(driver, jobTitle, location):
    jobURLs = []
    lastPage = "not last page"
    try:
        positionSearch = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, "KeywordSearch"))
        )
        locationSearch = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, "LocationSearch"))
        )
        print(locationSearch)
        heroSearchButton = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, "HeroSearchButton"))
        )
        locationSearch.clear()
        locationSearch.send_keys(location)
        positionSearch.send_keys(jobTitle)
        heroSearchButton.click()
        #nextPageBtn = driver.find_element_by_css_selector('li.next').click()
        print(lastPage)
        for x in range(10):
            # lastPageCheck = WebDriverWait(driver, 10).until(
            #     EC.presence_of_element_located((By.CSS_SELECTOR, ".page.current.last"))
            # ).get_attribute("class")
            # print(lastPage)
            # if lastPageCheck == "page current last":
            #     break
            # else:
            # elems = WebDriverWait(driver, 15).until(
            #     EC.presence_of_all_elements_located((By.CSS_SELECTOR, 'div.logoWrap>a'))
            # )

            # for elem in elems:
            #     jobURLs.append(elem.get_attribute("href"))
            #     #print(elem.get_attribute("href"))
            # #print(jobURLs)
            # print(len(jobURLs))

            WebDriverWait(driver, 30).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'li.next>a'))).click()
            #     elems = WebDriverWait(driver, 10).until(
            #         EC.presence_of_all_elements_located((By.CSS_SELECTOR, 'div.logoWrap>a'))
            #     )
            #     for elem in elems:
            #         jobURLs.append(elem.get_attribute("href"))
            #         print(elem.get_attribute("href"))
            #     WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'li.next>a'))).click()
            # else:
            #     elems = WebDriverWait(driver, 10).until(
            #         EC.presence_of_all_elements_located((By.CSS_SELECTOR, 'div.logoWrap>a'))
            #     )
            #     for elem in elems:
            #         jobURLs.append(elem.get_attribute("href"))
            #         print(elem.get_attribute("href"))
            #     print("No more pages left")
            #     break
        #print(len(jobURLs))
        # elems = WebDriverWait(driver, 10).until(
        #     EC.presence_of_all_elements_located((By.CSS_SELECTOR, 'div.logoWrap>a'))
        # )
        # for elem in elems:
        #     print(elem.get_attribute("href"))
    except:
        driver.quit()

start()

