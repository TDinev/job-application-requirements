from selenium.common.exceptions import NoSuchElementException, ElementClickInterceptedException
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
import time
import pandas as pd

def get_jobs(keyword, num_jobs, verbose):
    
    '''Gathers jobs as a dataframe, scraped from Glassdoor'''
    
    #Initializing the webdriver
    # options = webdriver.ChromeOptions()
    
    driver = webdriver.Firefox()
    driver.set_window_size(1120, 1000)

    url = 'https://www.glassdoor.com/Job/jobs.htm?sc.keyword="' + keyword
    driver.get(url)

    # Test for the "GDPR" prompt and get rid of it by agreeing.
    # Clicking it up to 3 times because of some "cool" script.
    try:
        while WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "onetrust-accept-btn-handler"))).text == "Accept Cookies":
            print(WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "onetrust-accept-btn-handler"))).text)
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "onetrust-accept-btn-handler"))).click()
    except ElementClickInterceptedException:
        pass

    jobs = []
    initialPageCounterText = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, "div[data-test='page-x-of-y'"))).text
    counter = 1

    while len(jobs) < num_jobs:
        if counter == 1:
            onePageLoop(driver, jobs, num_jobs)
        else:
            pageCounterText = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, "div[data-test='page-x-of-y'"))).text
            while pageCounterText == initialPageCounterText:
                pageCounterText = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, "div[data-test='page-x-of-y'"))).text
                continue
            onePageLoop(driver, jobs, num_jobs)

        #Clicking on the "next page" button
        try:
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, "li.next>a"))).click()
            counter += 1
        except NoSuchElementException:
            print("Scraping terminated before reaching target number of jobs. Needed {}, got {}.".format(num_jobs, len(jobs)))
            break
    
    print(pd.DataFrame(jobs))
    return pd.DataFrame(jobs)

def onePageLoop(driver, jobs, num_jobs):
    try:
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.CLASS_NAME, "selected"))).click()
    except ElementClickInterceptedException:
        pass

    time.sleep(.1)

    #Test for the "Sign Up" prompt and get rid of it.
    try:
        driver.find_element_by_css_selector(".SVGInline-svg.modal_closeIcon-svg").click()
    except NoSuchElementException:
        pass

    
    #Going through each job in this page
    job_buttons = WebDriverWait(driver, 10).until(EC.presence_of_all_elements_located((By.CLASS_NAME, "jl")))
    buttonsList = []
    for job_button in job_buttons:
        buttonsList.append(job_button.get_attribute("data-id"))

    for job_button in job_buttons:  
        jobId = job_button.get_attribute("data-id")
        print(jobId)
        print("Progress: {}".format("" + str(len(jobs)) + "/" + str(num_jobs)))
        if len(jobs) >= num_jobs:
            break

        job_button.click()

        collected_successfully = False
        jid = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, f"article[data-id='{jobId}'"))).get_attribute("data-id")
        while not collected_successfully and jobId == jid:
            try:
                company_name = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, './/div[@class="employerName"]'))).text
                location = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, './/div[@class="location"]'))).text
                job_title = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, './/div[contains(@class, "title")]'))).text
                job_description = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, './/div[@class="jobDescriptionContent desc"]'))).text
  
                jobs.append({
                    "Name" : company_name,
                    "Location" : location,
                    "Job Title" : job_title,
                    "Job_Description" : job_description,
                })    
                collected_successfully = True
            except:
                continue           
        
        print(jobs[-1])

df = get_jobs("biostatistician", 60, True)
df